# 1.4.3
* vaadin: 14.1.17
* tippy: 5.2.1
* new: changelog
* bugfix: NPE in Cleaner (#16)